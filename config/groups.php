<?php

return [
    'dev' => [
        'name' => 'dev',
        'god' => true
    ],
    'admin' => [
        'name' => 'Admin',
        'permissions' => [
            'admin' => [
                'groups' => ['view', 'add', 'edit', 'save', 'delete'],
                'settings' => ['view', 'edit', 'save'],
                'upload' => ['image'],
                'users' => ['view', 'add', 'edit', 'save', 'delete']
            ],
        ],
    ],
];
