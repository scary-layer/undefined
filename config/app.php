<?php

return [
    "name" => 'Undefined admin',
    'user-model' => '\App\User',
    'datetime-format' => 'd.m.Y H:i:s',
    'fallback-avatar' => '/assets/undefined/img/no-avatar.png'
];