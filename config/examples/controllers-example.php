<?php

return [
    'users/save' => [
        'type' => 'post',
        'controller' => '\ScaryLayer\Undefined\Controllers\UserController@save',
        'middleware' => 'undefined-permissions:admin.users.save',
    ]
];