<?php

return [
    'text' => [
        'col' => '',
        'type' => 'text',
        'label' => 'Text label'
    ],
    'number' => [
        'col' => 'col-xl-2',
        'type' => 'number',
        'label' => 'Number label'
    ],
    'email' => [
        'type' => 'email',
        'label' => 'Email label'
    ],
    'file' => [
        'type' => 'file',
        'label' => 'File label'
    ],
    'image' => [
        'type' => 'image',
        'label' => 'Image',
        'validation' => 'mimes:jpeg,jpg,png,gif|max:10000',
        'save_folder' => 'images',
        'options' => [
            'width' => 100,
            'height' => 100,
        ],
        'dublicate' => [
            'field' => 'image_min',
            'save_folder' => 'images/min',
            'options' => [
                'width' => 100,
                'height' => 100,
            ]
        ],
    ],
    'image-multiple' => [
        'type' => 'image-multiple',
        'label' => 'Image multiple label',
        'save' => [
            'model' => '\ScaryLayer\Undefined\Models\UserAdditionalImage',
            'field' => 'path',
            'relation_field' => 'user_id',
            'folder' => 'image-multiple',
        ],
        'options' => [
            'width' => 100,
            'height' => 100,
        ],
        'dublicate' => [
            'field' => 'path_min',
            'save_folder' => 'image-multiple/min',
            'options' => [
                'width' => 100,
                'height' => 100,
            ]
        ],
    ],
    'radio' => [
        'type' => 'radio',
        'label' => 'Radio label',
        'rows' => [
            [
                'text' => 'Radio 1',
                'value' => '0',
            ],
            [
                'text' => 'Radio 2',
                'value' => '2',
            ],
        ],
    ],
    'checkbox' => [
        'type' => 'checkbox',
        'label' => 'Checkbox label',
        'text' => 'Checkbox'
    ],
    'textarea' => [
        'type' => 'textarea',
        'label' => 'Textarea label',
    ],
    'select' => [
        'type' => 'select',
        'label' => 'Select label',
        'rows' => [
            [
                'text' => 'Option 1',
                'value' => 1,
            ],
            [
                'text' => 'Option 2',
                'value' => 2,
            ],
        ],
    ],
    'select-multiple' => [
        'type' => 'select-multiple',
        'label' => 'Select multiple label',
        'save' => [
            'model' => '\ScaryLayer\Undefined\Models\UserAdditionalImage',
            'field' => 'option_id',
            'relation_field' => 'user_id',
        ],
        'rows' => [
            [
                'text' => 'Option 1',
                'value' => 1,
            ],
            [
                'text' => 'Option 2',
                'value' => 2,
            ],
        ],
    ],
    'date' => [
        'type' => 'date',
        'label' => 'Date label',
        'value' => '2019-03-19'
    ],
    'datetime' => [
        'type' => 'datetime',
        'label' => 'Datetime label',
        'value' => '2019-03-19 23:17:01'
    ],
    'time' => [
        'type' => 'time',
        'label' => 'Time label',
        'value' => '23:17:01'
    ],
];