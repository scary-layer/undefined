<?php

return [
    "admin-sidebar" => [
        [
            'link' => '/admin',
            'title' => 'Home',
            'icon' => '<div class="icon dripicons-home"></div>'
        ],
        [
            'link' => '/admin/users',
            'title' => 'Users',
            'icon' => '<div class="icon dripicons-user"></div>',
            'permission' => 'admin.users.view',
            'submenu' => [
                [
                    'link' => '/admin/users',
                    'title' => 'List',
                    'icon' => '<div class="icon dripicons-view-list"></div>',
                    'permission' => 'admin.users.view'
                ],
                [
                    'link' => '/admin/groups',
                    'title' => 'Groups',
                    'icon' => '<div class="icon dripicons-user-group"></div>',
                    'permission' => 'admin.groups.view'
                ]
            ]
        ],
        [
            'link' => '/admin/settings',
            'title' => 'Settings',
            'icon' => '<div class="icon dripicons-gear"></div>',
            'permission' => 'admin.settings.view'
        ],
    ],
    "admin-user-menu" => [
        [
            'link' => '/',
            'title' => 'Back to site',
            'icon' => '<div class="icon dripicons-exit"></div>',
        ],
        [
            'link' => '/admin/logout',
            'title' => 'Logout',
            'icon' => '<div class="icon dripicons-skip"></div>',
        ],
    ],
];