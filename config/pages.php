<?php

return [
    'users' => [
        'title' => 'Users',
        'model' => config('undefined.user-model'),
        'in_modal' => true,
        'save-action' => 'users/save',
        'columns' => [
            [
                'name' => 'Avatar',
                'field' => 'avatar',
                'type' => 'image',
                'options' => [
                    'class' => 'col-xl-2',
                    'fallback' => config('undefined.fallback-avatar'),
                    'width' => '100px',
                ],
            ],
            [
                'name' => 'Name (human)',
                'field' => 'name',
                'sortable' => true,
            ],
            [
                'name' => 'Group',
                'field' => 'group',
                'blade' => "{{ Arr::get(\$row, \$column['field']) ? __('undefined::core.'. \ScaryLayer\Undefined\Models\Group::find(Arr::get(\$row, \$column['field']))['name']) : '' }}",
                'sortable' => true,
            ],
            [
                'name' => 'Email',
                'field' => 'email',
                'sortable' => true,
            ],
            [
                'name' => 'Created At',
                'field' => 'created_at',
                'type' => 'datetime',
                'sortable' => true,
            ]
        ],
        'actions' => [
            'edit',
            'delete',
        ],
        'searchable' => [
            'default' => ['id'],
            'like' => ['name', 'email']
        ],
        'form' => [
            'avatar' => [
                'type' => 'image',
                'label' => 'Avatar',
                'validation' => 'mimes:jpeg,jpg,png,gif|max:10000',
                'save_folder' => 'avatars',
            ],
            'name' => [
                'col' => '',
                'type' => 'text',
                'label' => 'Name',
                'validation' => 'required|min:3|max:255',
            ],
            'email' => [
                'type' => 'email',
                'label' => 'Email',
                'validation' => 'required|unique:users,email|max:255',
            ],
            'password' => [
                'type' => 'password',
                'label' => 'Password'
            ],
            'group' => [
                'type' => 'select',
                'label' => 'Group',
                'php' => '\ScaryLayer\Undefined\Models\Group::listForSelect()',
            ]
        ]
    ]
];
