<?php

return [
    'admin' => [
        'text' => 'Admin dashboard',
        'permissions' => [
            'groups' => [
                'text' => 'Groups',
                'permissions' => [
                    'view', 'add', 'edit', 'save', 'delete'
                ],
            ],
            'settings' => [
                'text' => 'Settings',
                'permissions' => [
                    'view', 'edit', 'save',
                ],
            ],
            'upload' => [
                'text' => 'Upload permissions',
                'permissions' => [
                    'image'
                ]
            ],
            'users' => [
                'text' => 'Users',
                'permissions' => [
                    'view', 'add', 'edit', 'save', 'delete'
                ],
            ],
        ],
    ]
];