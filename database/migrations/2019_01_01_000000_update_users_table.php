<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->nullable()->after('id');
            $table->string('group')->nullable()->after('avatar');
            $table->string('last_ip')->nullable()->after('remember_token');
            $table->string('last_login')->nullable()->after('last_ip');
            $table->boolean('new')->default(0)->after('last_login');
        });

        $user_class = config('undefined.user-model');

        // Create admin user
        $user = new $user_class;
        $user->group = 'admin';
        $user->name = 'admin';
        $user->email = 'admin@undefined.com';
        $user->password = Hash::make('password');
        $user->new = 1;
        $user->save();

        // Create dev user
        $user = new $user_class;
        $user->group = 'dev';
        $user->name = 'dev';
        $user->email = 'dev@undefined.com';
        $user->password = Hash::make('lumberjack');
        $user->new = 1;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('avatar');
            $table->dropColumn('group');
            $table->dropColumn('last_ip');
            $table->dropColumn('last_login');
            $table->dropColumn('new');
        });
    }
}
