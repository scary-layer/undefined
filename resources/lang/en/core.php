<?php

return [
    '#' => '#',
    'Actions' => 'Actions',
    'Add' => 'Add',
    'Add group' => 'Add group',
    'Admin' => 'Admin',
    'Admin dashboard' => 'Admin dashboard',
    'An error occurred while executing the request' => 'An error occurred while executing the request',
    'Are you sure?' => 'Are you sure?',
    'Avatar' => 'Avatar',
    'Back to site' => 'Back to site',
    'Cancel' => 'Cancel',
    'Change password' => 'Change password',
    'Change your password' => 'Change your password',
    'Choose file' => 'Выберите файл',
    'Created At' => 'Created At',
    'Delete' => 'Delete',
    'Edit' => 'Edit',
    'Edit group' => 'Edit group',
    'Edit profile' => 'Edit profile',
    'Email' => 'Email',
    'Enter search phrase' => 'Enter search phrase',
    'Failed to authenticate user with this credentials' => 'Failed to authenticate user with this credentials',
    'Group' => 'Group',
    'Group key' => 'Group key',
    'Group name' => 'Group name',
    'Groups' => 'Groups',
    'Hide' => 'Hide',
    'Hide / Show' => 'Hide / Show',
    'Home' => 'Home',
    'If you delete this entry you will not be able to access it in the future!' => 'If you delete this entry you will not be able to access it in the future!',
    'Let me back' => 'Let me back',
    'List' => 'List',
    'Log In' => 'Log In',
    'Login to admin dashboard' => 'Login to admin dashboard',
    'Logout' => 'Logout',
    'Logs' => 'Logs',
    'Name' => 'Name',
    'Name (human)' => 'Name (human)',
    'No data' => 'No data',
    'OK' => 'OK',
    'Password' => 'Password',
    'Password confirmation' => 'Password confirmation',
    'Рasswords do not match' => 'Рasswords do not match',
    'Рassword must be longer than 6 characters' => 'Рassword must be longer than 6 characters',
    'Per page' => 'Per page',
    'Personal area' => 'Personal area',
    'Record successfully deleted' => 'Record successfully deleted',
    'Request error!' => 'Request error!',
    'Save' => 'Save',
    'Search' => 'Search',
    'Search!' => 'Search',
    'Settings' => 'Settings',
    'Show' => 'Show',
    'Showing items' => 'Showing items from :from to :to of :of',
    'Successfully saved' => 'Successfully saved',
    'UI elements' => 'UI elements',
    'Upload permissions' => 'Upload permissions',
    'Users' => 'Users',

    # -------------------------------------------------------------------------

    'add (permissions)' => 'Add',
    'dev' => 'dev',
    'edit (permissions)' => 'Edit',
    'delete (permissions)' => 'Delete',
    'image (permissions)' => 'Image',
    'save (permissions)' => 'Save',
    'view (permissions)' => 'View',
];