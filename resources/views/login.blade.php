@extends('undefined::components.main')

@section('content')

<style>.btn-light {margin-right: 5px;} form {padding: 10px;}</style>

<div class="row justify-content-md-center align-items-center h-100">
    <div class="col-xl-3 block white-block">

        @utitle(['options' => ['title' => __('undefined::core.Login to admin dashboard')]])

        <form action="/admin/login" method="post">

            @csrf

            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="{{ __('undefined::core.Email') }}" required>
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="{{ __('undefined::core.Password') }}" required>
            </div>

            <div class="form-group">
                @if (session('error'))
                    <small class="text-danger">{{ session('error') }}</small>
                @endif
            </div>

            <div class="float-right">
                <a href="/" class="btn btn-light d-inline-block">{{ __('undefined::core.Let me back') }}</a>
                <button type="submit" class="btn btn-success float-right">{{ __('undefined::core.Log In') }}</button>
            </div>
            
        </form>

    </div>
</div>

@endsection