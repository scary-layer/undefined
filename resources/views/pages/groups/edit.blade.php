@extends('undefined::components.main')

@section('content')

    <style>
        .col-xl-3 {padding: 0}
        .permissions .block {margin-top:0; margin-right: 0}
        .permissions:nth-child(4) .block {margin-right: 10px}
    </style>
    <form data-action="/admin/groups/save" data-method="post">
        <div class="block white-block">

            @utitle(['options' => ['actions' => ['save']]])

            <div class="row form-content">

                @csrf

                @if (request('name'))
                    <input type="hidden" name="key" value="{{ request('name') }}">
                @else
                    <div class="form-group w-100">
                        <label>{{ __('undefined::core.Group key') }}</label>
                        <input type="text" name="key" placeholder="{{ __('undefined::core.Group key') }}" class="form-control">
                    </div>
                @endif

                <div class="form-group w-100">
                    <label>{{ __('undefined::core.Group name') }}</label>
                    <input type="text" name="name" placeholder="{{ __('undefined::core.Group name') }}" class="form-control" value="{{ $model['name'] }}">
                </div>
            </div>
        </div>

        @foreach ($permissions as $section => $data)

            <div class="block green-block">
                @utitle(['options' => ['title' => __('undefined::core.'. $data['text']), 'class' => 'without-underline']])
            </div>

            <div class="row">

                @foreach ($data['permissions'] as $page => $data)

                    @if (auth()->user()->may($section . '.' . $page))

                        <div class="col-xl-3 permissions">
                            <div class="block white-block h-100">
                                
                                @utitle(['options' => ['title' => __('undefined::core.'. $data['text'])]])

                                @foreach ($data['permissions'] as $item)

                                    @if ($item != 'save' && auth()->user()->may($section .'.'. $page .'.'. $item))

                                        @ucheckbox(['options' => [
                                            'name' => 'permissions['. $section .'.'. $page .'.'. $item . ']',
                                            'text' => __('undefined::core.'. $item .' (permissions)'),
                                            'value' => 1,
                                            'checked' => \ScaryLayer\Undefined\Models\Group::may($section .'.'. $page .'.'. $item, request('name'))
                                        ]])

                                    @endif
                                @endforeach

                            </div>
                        </div>

                    @endif

                @endforeach
                
            </div>

        @endforeach
    </form>

@endsection