@extends('undefined::components.main')

@section('content')

    <div class="block white-block">
        
        @utitle(['options' => [
            'route' => $page,
            'actions' => [
                'add', 'search', 'count'
            ]
        ]])

        <div class="table">
            <div class="row head">
                <a class="col d-block col-xl-1 id">{{ __("#") }}</a>
                <a class="col col-xl-2 d-block">{{ __("Name") }}</a>
                <a class="col d-block"></a>
                <div class="col actions">{{ __('undefined::core.Actions') }}</div>
            </div>

            @php $i = 1; @endphp
            @foreach ($data as $name => $item)

                @if (!Arr::get($item, 'god') || auth()->user()->isGod())

                    <div class="row">
                        <div class="col col-xl-1 id">{{ $i }}</div>
                        <div class="col col-xl-2">{{ __('undefined::core.'. $item['name']) }}</div>
                        <div class="col">

                            @php
                                $count = \ScaryLayer\Undefined\Models\Group::count($name);
                                $percent = $permissions_count > 0 ? ($count / $permissions_count) * 100 : 0;
                            @endphp

                            <div class="progress">
                                <div
                                    class="progress-bar"
                                    role="progressbar"
                                    style="width: {{ $percent }}%"
                                    aria-valuenow="{{ $count }}"
                                    aria-valuemin="0"
                                    aria-valuemax="{{ $permissions_count }}">
                                
                                </div>
                            </div>
                        </div>
                        <div class="col actions">
                            <a href="/admin/groups/edit?name={{ $name }}" class="icon text-blue clickable edit-button">
                                <i data-feather="edit"></i>
                            </a>
                            <div
                                class="icon text-red clickable delete-button" 
                                data-url="/admin/groups/delete"
                                data-id="{{ $name }}"
                                title="{{ __('undefined::core.Delete') }}">

                                <i data-feather="trash-2"></i>
                            </div>
                        </div>
                    </div>
                    @php $i++; @endphp

                @endif

            @endforeach

        </div>
    </div>

@endsection