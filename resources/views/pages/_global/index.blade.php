@extends('undefined::components.main')

@section('content')

    <div class="block white-block">
        
        @utitle(['options' => [
            'route' => $page,
            'actions' => [
                'add', 'search', 'count'
            ]
        ]])

        @utable(['options' => [
            'data' => $data,
            'route' => $page,
            'model' => $model,
            'columns' => $columns,
            'actions' => $actions,
        ]])
    </div>

@endsection