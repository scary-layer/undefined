@extends('undefined::components.main')

@section('content')

    <form data-action="{{ isset($action) ? $action : '/admin/'. $page .'/save' }}" data-method="post" enctype="multipart/form-data">
        <div class="block white-block">
            @utitle(['options' => ['actions' => ['save']]])
            <div id="pjax">{!! $html !!}</div>
        </div>
    </form>

@endsection