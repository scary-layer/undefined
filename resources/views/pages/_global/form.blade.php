<style>.form-content .col > div{padding-right: 10px;}</style>
<div class="row form-content">

    @csrf
    <input type="hidden" name="model" value="{{ $model_namespace }}">
    <input type="hidden" name="id" value="{{ request('id') }}">
    <input type="hidden" name="page" value="{{ $page }}">

    @foreach ($fields as $field => $options)

        @php
            $validation = isset($options['validation']) ? explode('|', $options['validation']) : [];
            
            $numeric_attrs = ['min' => 'minlength', 'max' => 'maxlength'];
            $data['required'] = in_array('required', $validation) ? 'required' : '';

            foreach ($numeric_attrs as $rule => $attr) {
                $item = collect($validation)->search(function ($item, $key) use ($rule) {
                    return mb_strpos(' '. $item, $rule .':');
                });

                $data[$attr] = '';
                if ($item) {
                    $parts = explode(':', $validation[$item]);
                    $data[$attr] = count($parts) > 1 ? $attr .'='. $parts[1] : '';
                }
            }

            $label = isset($options['label']) ? __('undefined::core.'. $options['label']) : null;
        @endphp
        
        <div class="col {{ $options['col'] ?? 'col-md-12' }}">

            @if ($options['type'] == 'hidden')

                <input type="hidden" name="{{ $field }}" value="{{ $options['value'] ?? $model->{$field} }}">

            @elseif ($options['type'] == 'text')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="text"
                        name="{{ $field }}"
                        placeholder="{{ $options['placeholder'] ?? null }}"
                        class="form-control {{ $options['class'] ?? null }}"
                        value="{{ $options['value'] ?? $model->{$field} }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>

                </div>

            @elseif ($options['type'] == 'email')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="email"
                        name="{{ $field }}"
                        placeholder="{{ $options['placeholder'] ?? null }}"
                        class="form-control {{ $options['class'] ?? null }}"
                        value="{{ $options['value'] ?? $model->{$field} }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>

                </div>

            @elseif ($options['type'] == 'password')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="password"
                        name="{{ $field }}"
                        placeholder="{{ $options['placeholder'] ?? null }}"
                        class="form-control {{ $options['class'] ?? null }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>

                </div>

            @elseif ($options['type'] == 'number')

            <div class="form-group">
                <label>{{ $label }}</label>
                <input
                    type="number"
                    name="{{ $field }}"
                    placeholder="{{ $options['placeholder'] ?? null }}"
                    class="form-control {{ $options['class'] ?? null }}"
                    value="{{ $options['value'] ?? $model->{$field} }}"
                    @if($options['step']) step="{{ $options['step'] }}" @endif
                    @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>

            </div>

            @elseif ($options['type'] == 'file')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    @ufile(['options' => ['name' => $field, 'value' => $options['value'] ?? $model->{$field}]])
                </div>

            @elseif ($options['type'] == 'image')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    @ufile(['options' => ['name' => $field, 'image' => true, 'value' => $options['value'] ?? $model->{$field}]])

            @elseif ($options['type'] == 'image-multiple')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    @ufilemultiple(['options' => ['name' => $field, 'image' => true, 'field' => $options['save']['field'], 'value' => $options['value'] ?? $model->{$field}]])
                </div>

            @elseif ($options['type'] == 'radio' && count(Arr::get($options, 'rows', [])))

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <div>

                        @foreach ($options['rows'] as $row)
                            @uradio(['options' => [
                                'name' => $field,
                                'text' => $row['text'],
                                'value' => $row['value'],
                                'checked' => $model->{$field} ?? Arr::get($row, 'default', [])
                            ]])
                        @endforeach

                    </div>
                </div>

            @elseif ($options['type'] == 'checkbox')

                <div class="form-group">
                    @if (Arr::get($options, 'label'))
                        <label>{{ $label }}</label>
                    @endif

                    @ucheckbox(['options' => [
                        'name' => $field,
                        'text' => $options['text'] ?? '',
                        'value' => $options['value'] ?? 1,
                        'checked' => $model->{$field} == Arr::get($options, 'value', 1)
                    ]])
                </div>

            @elseif ($options['type'] == 'textarea')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <textarea
                        placeholder="{{ $options['placeholder'] ?? null }}"
                        name="{{ $field }}"
                        class="form-control {{ $options['class'] ?? null }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
                        
                        {{ $model->{$field} ?? Arr::get($options, 'value') }}
                    </textarea>
                </div>

            @elseif ($options['type'] == 'select')

                @php 
                    if (isset($options['php'])) {
                        $options['rows'] = eval('return '. $options['php'] . ';');
                    } else {
                        $options['rows'] = Toolbox::listForSelect($options['model'], Arr::get($options, 'map.value', 'id'), Arr::get($options, 'map.text', 'name'), Arr::get($options, 'order.field', 'id'), Arr::get($options, 'order.direction'));
                    }
                @endphp

                @if (count(Arr::get($options, 'rows', [])))
                    
                    <div class="form-group">
                        <label>{{ $label }}</label>
                        <select name="{{ $field }}" class="form-control" {{ $data['required'] ?? '' }}>
                            
                            @foreach ($options['rows'] as $row)
                                <option value="{{ $row['value'] }}" {{ $row['value'] == ($options['value'] ?? $model->{$field}) ? 'selected' : '' }}>{{ $row['text'] }}</option>
                            @endforeach

                        </select>
                    </div>

                @endif

            @elseif ($options['type'] == 'select-multiple')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <select name="{{ $field }}[]" class="form-control" multiple>

                        @foreach ($options['rows'] as $row)
                            <option value="{{ $row['value'] }}" {{ isset($model->{$field}->keyBy('id')[$row['value']]) ? 'selected' : '' }}>{{ $row['text'] }}</option>
                        @endforeach

                    </select>
                </div>

            @elseif ($options['type'] == 'date')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="text"
                        class="form-control date-input {{ $options['class'] ?? null }}"
                        placeholder="{{ $options['placeholder'] ?? null }}"
                        name="{{ $field }}"
                        value="{{ $model->{$field} ?? Arr::get($options, 'value', date('Y-m-d')) }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach
                        {{ $data['required'] ?? '' }}>

                </div>

            @elseif ($options['type'] == 'datetime')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="text"
                        class="form-control datetime-input {{ $options['class'] ?? null }}"
                        name="{{ $field }}"
                        value="{{ $model->{$field} ?? Arr::get($options, 'value', date('Y-m-d')) }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach
                        {{ $data['required'] ?? '' }}>

                </div>

            @elseif ($options['type'] == 'time')

                <div class="form-group">
                    <label>{{ $label }}</label>
                    <input
                        type="text"
                        class="form-control time-input {{ $options['class'] ?? null }}"
                        name="{{ $field }}"
                        value="{{ $model->{$field} ?? Arr::get($options, 'value', date('Y-m-d')) }}"
                        @foreach (Arr::get($options, 'attr', []) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach
                        {{ $data['required'] ?? '' }}>

                </div>

            @endif

        </div>

    @endforeach

</div>