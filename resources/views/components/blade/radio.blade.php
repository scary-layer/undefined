<label class="radio d-table">
    <input 
        type="radio"
        name="{{ $options['name'] }}"
        class="d-none"
        value="{{ Arr::get($options, 'value') }}"

        @foreach (Arr::get($options, 'attributes', []) as $attr => $value)
            {{ $attr }}="{{ $value }}"
        @endforeach

        {{ Arr::get($options, 'checked') ? 'checked' : '' }}>

    <div class="replacer d-table-cell"><div class="insider"></div></div>
    <span class="text d-table-cell">{{ $options['text'] }}</span>
</label>