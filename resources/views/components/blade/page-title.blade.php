<div class="page-title {{ Arr::get($options, 'class') }}">
    {!! isset($options['title']) ? $options['title'] : $title !!}

    @if (isset($options['actions']))

        <div class="float-right">

            @if (in_array('count', $options['actions']))
                <span title="{{ __('undefined::core.Per page') }}">
                    <i class="clickable text-blue select-button" data-feather="list" data-route="{{ $options['route'] }}"></i>
                    @dropdownselect(['options' => [
                        ['value' => 10, 'text' => 10],
                        ['value' => 20, 'text' => 20],
                        ['value' => 30, 'text' => 30],
                        ['value' => 50, 'text' => 50],
                        ['value' => 100, 'text' => 100],
                    ]])
                </span>
            @endif

            @if (in_array('settings', $options['actions']))
                <i class="clickable text-blue settings-button" data-feather="settings" data-route="{{ $options['route'] }}"></i>
            @endif

            @if (in_array('search', $options['actions']))
                <span class="{{ request('search') ? 'notice' : '' }}" title="{{ __('undefined::core.Search') }}">
                    <i class="clickable text-green search-button" data-feather="search"></i>
                </span>
            @endif

            @if (in_array('add', $options['actions']) && auth()->user()->may('admin.'. $page .'.add'))

                @if (!$in_modal)

                    <a href="/admin/{{ $page }}/edit" class="d-inline text-blue clickable edit-button">
                        <i class="clickable text-blue" data-feather="plus"></i>
                    </a>

                @else

                    <div
                        class="d-inline text-blue clickable add-button" 
                        data-modal-path="/admin/{{ $page }}/edit"
                        data-route="{{ $page }}"
                        data-action="/admin/{{ $page }}/save"
                        data-title="{{ __('undefined::core.Add') }}"
                        title="{{ __('undefined::core.Add') }}">

                        <i class="clickable text-blue" data-feather="plus"></i>
                    </div>

                @endif

            @endif

            @if (in_array('save', $options['actions']) && (auth()->user()->may('admin.'. $page .'.add') || auth()->user()->may('admin.'. $page .'.edit')))
                <span class="text-green clickable submit save-button">
                    <i data-feather="save"></i>
                    <span>{{ __('undefined::core.Save') }}</span>
                </span>
            @endif
        </div>
        
    @endif
</div>
