@if (isset($options['image']))

    <div class="image-multiple-display" id="{{ $options['name'] ?? 'img' }}-display">

        @foreach ($options['value'] as $item)

            <img
                src="{{ $item[$options['field']] }}"
                alt=""
                title="">

        @endforeach

    </div>

@endif

<div class="custom-file">
    <input
        type="file"
        multiple
        name="{{ $options['name'] ?? 'img' }}[]"
        accept="{{ isset($options['image']) ? 'image/*' : '*' }}"
        class="custom-file-input {{ isset($options['image']) ? 'image-input-multiple' : 'file-input-multiple' }}"
        data-target_id="#{{ $options['name'] ?? 'img' }}-display">

    <label class="custom-file-label" for="file">{{ $options['placeholder'] ?? __('undefined::core.Choose file') }}</label>
</div>