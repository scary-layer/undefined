@if (isset($options))

    <div class="select-button-block hidden">

        @foreach ($options as $option)

            <div class="item {{ (!session('table-count') && $option['value'] == ScaryLayer\Undefined\Service\Table::$paginate) || session('table-count') == $option['value'] ? 'selected' : null }}" data-value="{{ $option['value'] }}">
                {{ $option['text'] }}
            </div>

        @endforeach

    </div>

@endif