@php 

use Illuminate\Support\Arr;

$id = [
    [
        'name' => '#',
        'field' => 'id',
        'sortable' => true,
        'options' => [
            'class' => 'col-xl-1 id'
        ],
    ]
];

@endphp

<div class="table">

    @if (isset($options['data']) && $options['data']->total() > 0)

        {{-- Table head generation --}}
        <div class="row head">

            @php $options['columns'] = array_merge($id, $options['columns']); @endphp

            @foreach ($options['columns'] as $column)

                @php
                    $direction = null;
                    if (request('sort') == $column['field']) {
                        $direction = request('order') == 'desc' ? 'asc' : 'desc';
                    }

                    $link = '?sort='. $column['field'] . '&order=' . $direction;
                @endphp

                <a {!! !Arr::get($column, 'sortable') ? : 'href="'. $link .'"' !!} class="col d-block {{ Arr::get($column, 'options.class') }}">
                    {{ __('undefined::core.'. $column['name']) }}
                    @if ($direction) {!! $direction == 'desc' ? '<i data-feather="arrow-down"></i>' : '<i data-feather="arrow-up"></i>' !!} @endif
                </a>

            @endforeach

            @if (isset($options['actions']))
                <div class="col actions">{{ __('undefined::core.Actions') }}</div>
            @endif

        </div>


        {{-- Table content generation --}}
        @foreach ($options['data'] as $row)

            <div class="row">

                @foreach ($options['columns'] as $column)

                    <div class="col {{ Arr::get($column, 'options.class') }}">

                        @if (isset($column['blade']))

                            {{ eval(Toolbox::renderBladeString($column['blade'])) }}

                        @elseif (Arr::get($column, 'type') == 'datetime')

                            @if (Arr::get($row, $column['field']))
                                {{ date(Arr::get($column, 'options.format') ?? config('undefined.datetime-format'), strtotime(Arr::get($row, $column['field']))) }}
                            @else
                                <p class="text-grey">-</p>
                            @endif

                        @elseif (Arr::get($column, 'type') == 'image')

                            <img src="{{ Arr::get($row, $column['field']) ?? Arr::get($column, 'options.fallback') }}" style="width: {{ Arr::get($column, 'options.width', '100%') }}" alt="">

                        @elseif (Arr::get($column, 'field'))

                            {{ Arr::get($row, $column['field']) }}

                        @endif
                    </div>

                @endforeach


                @if (isset($options['actions']))

                    <div class="col actions">

                        @if (isset($options['actions']['view']) && auth()->user()->may('admin.'. $page .'.view'))
                            <a href="{{ eval(Toolbox::renderBladeString($options['actions']['view'])) }}" target="_blank" class="icon text-green clickable" title="{{ __('undefined::core.Show') }}"><i data-feather="eye"></i></a>
                        @endif

                        @if (in_array('edit', $options['actions']) && auth()->user()->may('admin.'. $page .'.edit'))

                            @if (!$in_modal)
                                <a href="/admin/{{ $page }}/edit?id={{ Arr::get($row, 'id') }}" class="icon text-blue clickable edit-button">
                                    <i data-feather="edit"></i>
                                </a>
                            @else
                                <div
                                    class="icon text-blue clickable edit-button" 
                                    data-modal-path="/admin/{{ $page }}/edit?id={{ Arr::get($row, 'id') }}"
                                    data-route="{{ $page }}"
                                    data-action="/admin/save"
                                    data-title="Edit"
                                    title="{{ __('undefined::core.Edit') }}">
                                    
                                    <i data-feather="edit"></i>
                                </div>
                            @endif

                        @endif

                        @if (in_array('delete', $options['actions']) && auth()->user()->may('admin.'. $page .'.delete'))
                            <div
                                class="icon text-red clickable delete-button" 
                                data-model="{{ $options['model'] }}"
                                data-url="/admin/{{ $page }}/delete"
                                data-id="{{ $row['id'] }}"
                                title="{{ __('undefined::core.Delete') }}">

                                <i data-feather="trash-2"></i>
                            </div>
                        @endif

                    </div>

                @endif
                
            </div>

        @endforeach

    @else
        <p class="col text-center no-data">-- {{ __('undefined::core.No data') }} --</p>
    @endif


    {{-- Pagination --}}
    @if (isset($options['data']) && $options['data']->total() > $options['data']->perPage())

        <div class="row info-line">

            <div class="col">
                {{ __('undefined::core.Showing items', [
                    'from' => ($options['data']->currentPage() - 1) * $options['data']->perPage() + 1,
                    'to' => ($options['data']->currentPage() - 1) * $options['data']->perPage() + $options['data']->count(),
                    'of' => $options['data']->total()
                ]) }}
            </div>

            <div class="col">
                {{ $options['data']->appends([
                    'sort' => request('sort'),
                    'order' => request('order'),
                    'count' => request('count'),
                    'search' => request('search'),
                ])->links() }}
            </div>
            
        </div>

    @endif

</div>