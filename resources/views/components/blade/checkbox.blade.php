<label class="checkbox d-table">
    <input 
        type="checkbox"
        name="{{ $options['name'] }}"
        class="form-control d-none"
        value="{{ $options['value'] ?? '1' }}"

        @foreach (Arr::get($options, 'attributes', []) as $attr => $value)
            {{ $attr }}="{{ $value }}"
        @endforeach

        {{ Arr::get($options, 'checked') ? 'checked' : '' }}>

    <div class="replacer d-table-cell text-center"><i data-feather="check"></i></div>
    <span class="text d-table-cell">{{ $options['text'] }}</span>
</label>