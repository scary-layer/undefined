@if (isset($options['image']))

    <img
        src="{{ $options['value'] }}"
        alt=""
        title=""
        id="{{ $options['name'] ?? 'img' }}-display" 
        style="max-width: 100%; padding-bottom: 5px">

@else

    <div class="display-file row align-items-center {{ isset($options['value']) ? '' : 'hidden' }}" id="{{ $options['name'] ?? 'img' }}-display">
        
        <div class="col col-lg-2 icon">
            <i data-feather="file"></i>
        </div>

        <div class="col text">

            @if (isset($options['value']))

                @php
                    $parts = explode('/', $options['value']);
                    $string = array_pop($parts);
                    $parts = explode('-', $string);
                    $string = array_pop($parts);
                @endphp

                {{ $string }}

            @endif

        </div>
    </div>

@endif

<div class="custom-file">
    <input
        type="file"
        name="{{ $options['name'] ?? 'img' }}"
        accept="{{ isset($options['image']) ? 'image/*' : '*' }}"
        class="custom-file-input {{ isset($options['image']) ? 'image-input' : 'file-input' }}"
        data-target_id="#{{ $options['name'] ?? 'img' }}-display">

    <label class="custom-file-label" for="file">{{ $options['placeholder'] ?? __('undefined::core.Choose file') }}</label>
</div>