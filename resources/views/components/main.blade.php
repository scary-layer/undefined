<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? 'Undefined' }}</title>

    <link rel="icon" href="/assets/undefined/img/favicon.ico" sizes="16x16 32x32" type="image/png">
    <link href="/assets/undefined/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>

    <div class="row main-container h-100 w-100">

        @if (!isset($sidebar_hide))
            <!-- Sidebar -->
            @include('undefined::components.sidebar')
        @endif

        <!-- Content -->
        <div class="col content scrollable h-100">
            @yield('content')
        </div>

    </div>


    <div id="modals"></div>

    @include('undefined::components.translations')

    <script src="/assets/undefined/js/app.js"></script>
    @yield('script')
</body>
</html>