@php
    $_fields = [
        'An error occurred while executing the request',
        'Are you sure?',
        'Cancel',
        'Enter search phrase',
        'If you delete this entry you will not be able to access it in the future!',
        'OK',
        'Record successfully deleted',
        'Request error!',
        'Search!'
    ];
@endphp

<script>
    var translations = {
        @foreach($_fields as $_field)
        '{{ $_field }}': "{{ __('undefined::core.'. $_field) }}",
        @endforeach
    }

    function __(key){
        return translations[key] ? translations[key] : key;
    }
</script>