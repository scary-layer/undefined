<div class="modal" id="{{ $route && $type ? $route . '-' . $type : 'modal' }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered {{ $size ?? null }}" role="document">
        <form data-action="{{ $action }}" data-method="{{ $method ?? 'post' }}" class="modal-content" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ $title ?? 'Modal' }}</h5>
            </div>
            <div class="modal-body">
                @yield('modal-content')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{ __('undefined::core.Cancel') }}</button>
                <button type="button" class="modal-submit-btn btn btn-success">{{ __('undefined::core.Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>App.init();App.initDateInputs();</script>