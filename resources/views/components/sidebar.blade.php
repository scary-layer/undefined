<div class="col sidebar {{ $_COOKIE['sidebar'] ?? 'expanded' }} h-100">

    <div class="sidebar-header notice">

        <div class="img">
            <div>
                <img src="{{ Auth::user()->avatar ?? config('undefined.fallback-avatar') }}" alt="">
            </div>
        </div>

        <div class="name">{{ Auth::user()->name }}</div>
    </div>

    <div class="header-submenu submenu">

        @foreach (config('undefined-menus.admin-user-menu') as $item)

            @if (!isset($item['permission']) || auth()->user()->may($item['permission']))

                <a  href="{{ $item['link'] }}" class="item {{ Request::is(substr($item['link'], 1)) ? 'active' : null }}"
                    title="{{ __('undefined::core.'. $item['title']) }}">
                    {!! Arr::get($item, 'icon') !!}
                    <div class="text">{{ __('undefined::core.'. $item['title']) }}</div>
                </a>

            @endif

        @endforeach

    </div>

    <div class="sidebar-menu">

        @foreach (config('undefined-menus.admin-sidebar') as $item)

            @if (!isset($item['permission']) || auth()->user()->may($item['permission']))

                <a  href="{{ $item['link'] }}" class="item {{ Request::is(substr($item['link'], 1)) ? 'active' : null }}"
                    title="{{ __('undefined::core.'. $item['title']) }}">
                    
                    {!! Arr::get($item, 'icon') !!}
                    <div class="text">{{ __('undefined::core.'. $item['title']) }}</div>

                    @php $allowed_subitems = 0; @endphp

                    @if (isset($item['submenu']))
                    
                        @php
                            $allowed_subitems = collect($item['submenu'])->filter(function ($subitem, $key) use ($item) {
                                return (!isset($subitem['permission']) || auth()->user()->may($subitem['permission'])) && $item['link'] != $subitem['link'];
                            })->all();
                        @endphp

                        @if (count($allowed_subitems) > 0)
                            <div class="icon submenu-icon dripicons-chevron-right"></div>
                        @endif

                    @endif
                    
                </a>

                @if (isset($item['submenu']) && count($item['submenu']) > 0)

                    <div class="submenu">

                        @foreach ($item['submenu'] as $subitem)

                            @if ((!isset($subitem['permission']) || auth()->user()->may($subitem['permission'])) && (count($allowed_subitems) > 0 || $item['link'] != $subitem['link']))

                                <a  href="{{ $subitem['link'] }}" class="item {{ Request::is(substr($subitem['link'], 1)) ? 'active' : null }}"
                                    title="{{ __('undefined::core.'. $subitem['title']) }}">
                                    {!! Arr::get($subitem, 'icon') !!}
                                    <div class="text">{{ __('undefined::core.'. $subitem['title']) }}</div>
                                </a>

                            @endif

                        @endforeach

                    </div>

                @endif
                
            @endif

        @endforeach

    </div>

    <div class="item" id="expand-sidebar" title="{{ __('undefined::core.Hide / Show') }}">
        <div class="icon {{ !isset($_COOKIE['sidebar']) || $_COOKIE['sidebar'] == 'expanded' ? 'dripicons-chevron-left' : 'dripicons-chevron-right' }}"></div>
        <div class="text">{{ __('undefined::core.Hide') }}</div>
    </div>
    
</div>