@extends('undefined::components.main')

@section('content')

    <div class="row">
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Text input:</label>
                    <input type="text" placeholder="Text" class="form-control">
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode('<div class="form-group"><label>Textinput:</label><input type="text" placeholder="Text" class="form-control"></div>') !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Number input:</label>
                    <input type="number" placeholder="Number" class="form-control">
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode('<div class="form-group"><label>Number:</label><input type="number" placeholder="Number" class="form-control"></div>') !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>File input:</label>
                    @ufile(['options' => []])
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>File input:</label><div>@@ufile(['options' => []])</div></div>") !!}
                </code>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Time:</label>
                    <input type="text" class="form-control time-input" name="time" value="12:10:01">
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>Time:</label><input type=\"text\" class=\"form-control time-input\" name=\"time\" value=\"12:10:01\"></div>") !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Date:</label>
                    <input type="text" class="form-control date-input" name="date" value="2019-03-18">
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>Date:</label><input type=\"text\" class=\"form-control date-input\" name=\"date\" value=\"2019-03-18\"></div>") !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>DateTime:</label>
                    <input type="text" class="form-control datetime-input" name="datetime" value="2019-03-18 12:10:01">
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>DateTime:</label><input type=\"text\" class=\"form-control datetime-input\" name=\"datetime\" value=\"2019-03-18 12:10:01\"></div>") !!}
                </code>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Textarea:</label>
                    <textarea placeholder="Textarea" class="form-control"></textarea>
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode('<div class="form-group"><label>Textarea:</label><textarea placeholder="Textarea" class="form-control"></textarea></div>') !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Radio input:</label>
                    <div>
                        @uradio(['options' => ['name' => 'radio_1', 'text' => 'Radio 1']])
                        @uradio(['options' => ['name' => 'radio_1', 'text' => 'Radio 2']])
                    </div>
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>Radio input:</label><div>@@uradio(['options' => ['name' => 'radio_1', 'text' => 'Radio 1']])</div><div>@@uradio(['options' => ['name' => 'radio_1', 'text' => 'Radio 2']])</div></div>") !!}
                </code>
            </div>
        </div>
    
        <div class="col col-xl-4">
            <div class="block white-block">
                <div class="form-group">
                    <label>Checkbox:</label>
                    @ucheckbox(['options' => ['name' => 'checkbox_1', 'text' => 'Checkbox 1']])
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode("<div class=\"form-group\"><label>Checkbox:</label><div>@@ucheckbox(['options' => ['name' => 'checkbox_1', 'text' => 'Checkbox 1']])>/div></div>") !!}
                </code>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="block white-block">
                <div class="form-group">
                    <label>WYSIWYG:</label>
                    <textarea name="wysiwyg" class="wysiwyg"></textarea>
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode('<div class="form-group"><label>WYSIWYG:</label><textarea name="wysiwyg" class="wysiwyg"></textarea></div>') !!}
                </code>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="block white-block">
                <div class="form-group">
                    <label>CodeMirror:</label>
                    <textarea name="codemirror" class="codemirror"></textarea>
                </div>
                <code class="language-html" data-lang="html">
                    {!! Toolbox::printCode('<div class="form-group"><label>CodeMirror:</label><textarea name="codemirror" class="codemirror"></textarea></div>') !!}
                </code>
            </div>
        </div>
    </div>

</div>

@endsection