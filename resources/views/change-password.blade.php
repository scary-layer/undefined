@extends('undefined::components.main')

@section('content')

<style>.btn-light {margin-right: 5px;} form {padding: 10px;}</style>

<div class="row justify-content-md-center align-items-center h-100">
    <div class="col-xl-4 block white-block">

        @utitle(['options' => ['title' => __('undefined::core.Change your password')]])

        <form action="/admin/change-password" method="post" id="change-password-form">

            @csrf

            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="{{ __('undefined::core.Password') }}">
            </div>

            <div class="form-group">
                <input type="password" name="password-confirmation" class="form-control" placeholder="{{ __('undefined::core.Password confirmation') }}">
                <small id="change-password-error" class="text-danger"></small>
            </div>


            <div class="float-right">
                <button type="button" class="btn btn-success float-right">{{ __('undefined::core.Save') }}</button>
            </div>
            
        </form>

    </div>
</div>

@endsection


@section('script')

<script>
    $(document).ready(function() {

        $('button').click(function() {
            var input_one = $('input[name="password"]').val();
            var input_two = $('input[name="password-confirmation"]').val();

            if (input_one.length < 6) {
                $('#change-password-error').text('{{ __("undefined::core.Рassword must be longer than 6 characters") }}');
            } else if (input_one !== input_two) {
                $('#change-password-error').text('{{ __("undefined::core.Рasswords do not match") }}');
            } else {
                $('#change-password-form').submit();
            }
        });

    });
</script>

@endsection