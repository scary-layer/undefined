$(document).ajaxError(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {
    var response = xhr.responseText;
    App.notify(__("Request error!") + " " + thrownError + "\n" + response.substring(0, 500) + (response.length > 500 ? '...' : ''), 'error');
    console.log(xhr.responseJSON);
});

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});