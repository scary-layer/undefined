const LOCALE = document.querySelector('html').lang;

require('./bootstrap');
require('jquery-validation');
if (LOCALE != 'en') {
    require('jquery-validation/dist/localization/messages_' + LOCALE);
}

window.Noty = require('noty');
window.Cookies = require('browser-cookies');
window.CodeMirror = require('codemirror');
window.Feather = require('feather-icons');
window.slugify = require('slugify');

require('flatpickr');
require('mo-js');
require('select2');
require('sweetalert');

require('trumbowyg');
if (LOCALE != 'en') {
    require('trumbowyg/dist/langs/' + LOCALE + '.js');
}
require('trumbowyg/dist/plugins/cleanpaste/trumbowyg.cleanpaste.min.js');
require('trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js');
require('trumbowyg/dist/plugins/emoji/trumbowyg.emoji.min.js');
require('trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js');
require('trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.min.js');
require('trumbowyg/dist/plugins/table/trumbowyg.table.min.js');
require('trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js');

require('codemirror/mode/htmlmixed/htmlmixed.js');

const LANG = require("flatpickr/dist/l10n/"+ (LOCALE == "en" ? "default" : LOCALE) +".js").default.ru;
flatpickr.localize(LANG);

require('./main');
require('./additional');
require('./ready');