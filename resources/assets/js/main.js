class App
{
    static init()
    {
        Feather.replace();
        $('select').select2({minimumResultsForSearch: 20});

        if ($('.wysiwyg').length > 0) {
            App.initWYSIWYG();
        }

        if ($('.codemirror').length > 0) {
            App.initCodeMirror();
        }
          
        $(".image-input").change(function() {
            App.readURL(this, $(this).data('target_id'));
        });

        $(".image-input-multiple").change(function() {
            App.readURLs(this, $(this).data('target_id'));
        });

        $(".file-input").change(function(e) {
            var target = $($(this).data('target_id'));
            target.removeClass('hidden');
            target.find('.text').html(e.target.files[0].name);
        });

        $("input.slugify").change(function() {
            var target = $($(this).data('target_id'));
            target.val(slugify($(this).val(), {lower: true}));
        });
    }


    static initDateInputs()
    {
        flatpickr('.date-input', {
            allowInput: true,
            altInput: true,
            altFormat: 'd.m.Y',
            dateFormat: "Y-m-d",
        });

        flatpickr('.datetime-input', {
            allowInput: true,
            enableTime: true,
            altInput: true,
            altFormat: 'd.m.Y H:i',
            dateFormat: "Y-m-d H:i:S",
            time_24hr: true
        });

        flatpickr('.time-input', {
            allowInput: true,
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true
        });
    }


    static initPageTitle()
    {
        $('.submit').off('click').click(function(e) {
            e.preventDefault();
            
            var form = $(this).closest('form');
            form.validate();
    
            if (form.valid()) {
                App.sendRequest(form.data('method'), form.data('action'), new FormData(form[0]), function(data) {
                    App.pjax('#pjax');
                });
            }
        });
    
        $('.add-button').off('click').click(function() {
            var route = $(this).data('route');
            var title = $(this).data('title');
            var modal = $(this).data('modal-path');
            App.getModal(route, 'add', title, modal);
        });
    
        $('.select-button').off('click').click(function() {
    
            if ($(this).parent().find('.select-button-block').hasClass('hidden')) {
                $(this).parent().find('.select-button-block').removeClass('hidden');
            } else {
                $(this).parent().find('.select-button-block').addClass('hidden');
            }
    
        });
    
        $('.select-button-block > .item').off('click').click(function() {
            var url = new URL(window.location.href);
            url.searchParams.set('count', $(this).data('value'));
            window.location.href = url;
        });
    
        $('.search-button').off('click').click(function() {
    
            var url = new URL(window.location.href),
                search = url.searchParams.get("search");
    
            swal({
                title: __('Enter search phrase'),
                content: {
                    element: "input",
                    attributes:{
                        value: search,
                    },
                },
                button: {
                  text: __("Search!"),
                  closeModal: false,
                },
            })
            .then(name => {
                url.searchParams.set('search', '');
                if (name) {
                    url.searchParams.set('search', name);
                }
    
                window.location.href = url;
            })
        });
    }


    static initTable()
    {
        $('.settings-button').off('click').click(function() {
            var route = $(this).data('route');
            var title = 'Table settings';
            App.getModal(route, 'settings', title);
        });

        $('.edit-button').off('click').click(function() {
            var route = $(this).data('route');
            var title = $(this).data('title');
            var modal = $(this).data('modal-path');
            App.getModal(route, 'edit', title, modal);
        });
    
        $('.delete-button').off('click').click(function() {
            var id = $(this).data('id'),
                model = $(this).data('model'),
                url = $(this).data('url');
    
            swal({
                title: __("Are you sure?"),
                text: __("If you delete this entry you will not be able to access it in the future!"),
                icon: "warning",
                buttons: [__("Cancel"), __("OK")],
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.post(url, {id: id, model: model}, function(response) {
                        if (response.success) {
                            swal(__("Record successfully deleted"), {
                                icon: "success",
                            });

                            App.pjax('.content');
                        }
                    });
                }
            });
        });
    }


    static initWYSIWYG()
    {
        $.trumbowyg.svgPath = '/assets/undefined/svg/icons.svg';
        $('.wysiwyg').trumbowyg({
            lang: document.querySelector('html').lang,
            btns: [
                ['viewHTML'],
                ['undo', 'redo'],
                ['formatting', 'foreColor', 'backColor'],
                ['strong', 'em', 'del'],
                ['fontfamily', 'fontsize'],
                ['superscript', 'subscript'],
                ['link', 'emoji'],
                ['insertImage', 'upload'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['table'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ],
            plugins: {
                upload: {
                    serverPath: '/admin/upload/image',
                }
            }
        });
    }


    static initCodeMirror()
    {
        $('.codemirror').each(function() {
            var code = CodeMirror.fromTextArea(this, {
                lineNumbers: true,
                matchBrackets: true
            });

            code.on('change', function() {
                code.save();
            });
        });
    }


    static getModal(route, type, title, modal_path)
    {
        $.get((modal_path), {route: route, type: type, title: title}, function(data) {

            $('#modals').html(data);

            var modal = $('#' + route + '-' + type);
            modal.modal();

            // Modal submit action
            modal.find('.modal-submit-btn').click(function() {

                var form = modal.find('form');

                form.validate();

                if (form.valid()) {
                    App.sendRequest(form.data('method'), form.data('action'), new FormData(form[0]), function(data) {
                        if (data['close']) {
                            App.pjax('.content');
                            modal.modal('toggle');
                        }
                    });
                }

            });

        });
    }


    static sendRequest(type, url, data, success_function)
    {
        $.ajax({
            type: type,
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {

                if (!data['success']) {
                    App.notify(__('An error occurred while executing the request'), 'error');
                } else {

                    success_function(data);

                    if (data['noty']) {
                        App.notify(data['noty']['text'], data['noty']['type']);
                    } else if (data['redirect']) {
                        window.location.href = data['redirect'];
                    } else if (data['reload']) {
                        location.reload();
                    }
                }

            },
        });
    }


    static readURL(input, target)
    {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
      
            reader.onload = function(e) {
                $(target).attr('src', e.target.result);
            }
      
            reader.readAsDataURL(input.files[0]);
        }
    }

    static readURLs(input, target)
    {
        if (input.files) {

            $(target).html('');

            for (let i = 0; i < input.files.length; i++) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    $(target).append('<img src="' + e.target.result + '" alt="" title="">');
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    }


    static pjax(selector, options = {})
    {
        var url = options['url'] ? options['url'] : location.href;

        $.get(url, function(html) {
            
            var html = $(html).find(selector).html();
            $(selector).html(html);

            App.init();
            App.initPageTitle();
            App.initTable();
        });
    }


    static sidebar()
    {
        if ($('#expand-sidebar').find('.icon').hasClass('dripicons-chevron-right')) {
            $('#expand-sidebar').find('.icon').removeClass('dripicons-chevron-right');
            $('#expand-sidebar').find('.icon').addClass('dripicons-chevron-left');

            $('.sidebar').removeClass('minimized');
            $('.sidebar').addClass('expanded');
            Cookies.set('sidebar', 'expanded', {secure: false, httponly: false, path: '/', domain: window.location.hostname, expires: 365});
        } else {
            $('#expand-sidebar').find('.icon').removeClass('dripicons-chevron-left');
            $('#expand-sidebar').find('.icon').addClass('dripicons-chevron-right');

            $('.sidebar').removeClass('expanded');
            $('.sidebar').addClass('minimized');
            Cookies.set('sidebar', 'minimized', {secure: false, httponly: false, path: '/', domain: window.location.hostname, expires: 365});
        }
    }


    static notify(text, type, layout = 'bottomRight')
    {
        new Noty({
            layout: layout,
            type: type,
            text: text,
            timeout: 10000,
            animation: {
                open: function (promise) {
                    var n = this;
                    var Timeline = new mojs.Timeline();
                    var body = new mojs.Html({
                        el: n.barDom,
                        x: {500: 0, delay: 0, duration: 500, easing: 'elastic.out'},
                        isForce3d : true,
                        onComplete: function () {
                            promise(function(resolve) {
                                resolve();
                            })
                        }
                    });
        
                    var parent = new mojs.Shape({
                        parent: n.barDom,
                        width: 200,
                        height: n.barDom.getBoundingClientRect().height,
                        radius: 0,
                        x: {[150]: -150},
                        duration: 1.2 * 500,
                        isShowStart: true
                    });
        
                    n.barDom.style['overflow'] = 'visible';
                    parent.el.style['overflow'] = 'hidden';
        
                    var burst = new mojs.Burst({
                        parent: parent.el,
                        count: 10,
                        top: n.barDom.getBoundingClientRect().height + 75,
                        degree: 90,
                        radius: 75,
                        angle: {[-90]: 40},
                        children: {
                            fill: '#EBD761',
                            delay: 'stagger(500, -50)',
                            radius: 'rand(8, 25)',
                            direction: -1,
                            isSwirl: true
                        }
                    });
        
                    var fadeBurst = new mojs.Burst({
                        parent: parent.el,
                        count: 2,
                        degree: 0,
                        angle: 75,
                        radius: {0: 100},
                        top: '90%',
                        children: {
                            fill: '#EBD761',
                            pathScale: [.65, 1],
                            radius: 'rand(12, 15)',
                            direction: [-1, 1],
                            delay: .8 * 500,
                            isSwirl: true
                        }
                    });
        
                    Timeline.add(body, burst, fadeBurst, parent);
                    Timeline.play();
                },
                close: function (promise) {
                    var n = this;
                    new mojs.Html({
                        el: n.barDom,
                        x: {0: 500, delay: 10, duration: 500, easing: 'cubic.out'},
                        skewY: {0: 10, delay: 10, duration: 500, easing: 'cubic.out'},
                        isForce3d: true,
                        onComplete: function () {
                            promise(function(resolve) {
                                resolve();
                            })
                        }
                    }).play();
                }
            }
        }).show();
    }
}


window.App = App;