$(document).ready(function() {

    $('#expand-sidebar').click(function() {
        App.sidebar();
    });
    
    App.init();
    App.initDateInputs();
    App.initPageTitle();
    App.initTable();
});