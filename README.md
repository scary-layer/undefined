# Installation:

1.  Configure the laravel project and it's database.
2.  Run in project directory:
*  `composer require scary-layer/undefined`
*  `php artisan vendor:publish --provider="ScaryLayer\Undefined\Providers\ServiceProvider"`
*  `php artisan migrate` - it will also create admin and dev user.
3.  Edit App\User model.
*  Include ScaryLayer\Undefined\Traits\Permissions by inserting `use ScaryLayer\Undefined\Traits\Permissions;` to top of the file.
*  Add `Permissions` as App\User trait by inserting it to class requiries.
*  Also add field `group` to fillable array.

You must have something like:

```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;  
use Illuminate\Contracts\Auth\MustVerifyEmail;  
use Illuminate\Foundation\Auth\User as Authenticatable;  

use ScaryLayer\Undefined\Traits\Permissions;

class User extends Authenticatable
{
    use Notifiable, Permissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
```

### Congrats, you have successfully installed Undefined Laravel Admin!
If you encounter any problems, please, notify us. You can do that by creating an issue or by sending email to maintainer.
