const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', '../../../public/assets/undefined/css')
    .js('resources/assets/js/app.js', 'resources/publishable/js')
    .copy('../../../public/assets/undefined/css/app.css', 'resources/publishable/css')
    .copy('resources/publishable/js/app.js', '../../../public/assets/undefined/js');
