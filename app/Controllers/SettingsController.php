<?php

namespace ScaryLayer\Undefined\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ScaryLayer\Undefined\Service\Config;

class SettingsController extends Controller
{
    public function index()
    {
        return view('undefined::pages._global.form-page', [
            'title' => __('undefined::core.Settings'),
            'action' => '/admin/settings/save',
            'page' => 'settings',
            'html' => view('undefined::pages._global.form', [
                'page' => 'settings',
                'model_namespace' => Config::class,
                'model' => config('undefined-settings'),
                'fields' => config('undefined-settings')
            ])->render()
        ]);
    }

    public function save(Request $request)
    {
        $items = config('undefined-settings');
        foreach ($request->all() as $key => $setting) {
            if (config('undefined-settings.'. $key)) {
                $items = array_replace_recursive($items, [$key => ['value' => $setting]]);
            }
        }

        Config::edit($items, 'undefined-settings');

        return [
            'success' => true,
            'noty' => [
                'type' => 'success',
                'text' => __('undefined::core.Successfully saved')
            ]
        ];
    }
}