<?php

namespace ScaryLayer\Undefined\Controllers;

use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use ScaryLayer\Undefined\Models\Setting;
use ScaryLayer\Undefined\Service\Config;
use ScaryLayer\Undefined\Service\ImageHelper;
use ScaryLayer\Undefined\Service\Table;
use Validator;

class GlobalController extends Controller
{
    public function index(Request $request)
    {
        $this->middleware('undefined-permissions:admin.' . $request->page_name . '.view');

        $config = config('undefined-pages.' . $request->page_name);
        $data = Table::process($request, $request->page_name, new $config['model'] ?? null);

        return view('undefined::pages._global.index', [
            'title' => __('undefined::core.' . ($config['title'] ?? 'Global page')),
            'page' => $request->page_name,
            'model' => $config['model'] ?? null,
            'data' => $data,
            'columns' => config('undefined-pages.' . $request->page_name . '.columns'),
            'actions' => config('undefined-pages.' . $request->page_name . '.actions'),
            'in_modal' => $config['in_modal'] ?? null,
        ]);
    }


    public function edit(Request $request)
    {
        $this->middleware('undefined-permissions:admin.' . $request->page . ($request->has('id') ? '.edit' : '.add'));

        $config = config('undefined-pages.' . $request->page);

        $type = 'add';
        $item = new $config['model'];

        if ($request->has('id')) {
            $type = 'edit';
            $item = $item->find($request->id);
        }

        if ($config['in_modal']) {
            return view('undefined::pages._global.modal', [
                'title' => $request->title,
                'action' => config('undefined-pages.' . $request->page . '.save-action') ?? '/admin/save',
                'route' => $request->page,
                'type' => $type,
                'html' => (string) view('undefined::pages._global.form', ['page' => $request->page, 'model_namespace' => $config['model'], 'model' => $item, 'fields' => $config['form'] ?? null])
            ]);
        }

        return view('undefined::pages._global.form-page', [
            'title' => __('undefined::core.Edit'),
            'page' => $request->page,
            'html' => (string) view('undefined::pages._global.form', ['page' => $request->page, 'model_namespace' => $config['model'], 'model' => $item, 'fields' => $config['form'] ?? null])
        ]);
    }


    public function save(Request $request)
    {
        $this->middleware('undefined-permissions:admin.' . $request->page . '.save');

        $validator = Validator::make($request->all(), Config::generateValidation($request->page, $request->id));
        if ($validator->fails()) {
            return ['success' => true, 'noty' => ['text' => $validator->errors()->all()[0], 'type' => 'error']];
        }

        $item = $request->id ? (new $request->model)->find($request->id) : (new $request->model);
        $item->fill($request->all());

        foreach (config('undefined-pages.' . $request->page . '.form') as $field => $config) {

            $value = $request->{$field};

            if ($config['type'] == 'password') {
                $item->{$field} = Hash::make($value);
            } elseif (in_array($config['type'], ['file', 'image']) && $value) {
                $item->{$field} = ImageHelper::process($value, $config['save_folder'], $config['options'] ?? null);
                if (isset($config['dublicate'])) {
                    $item->{$config['dublicate']['field']} = ImageHelper::process($value, $config['dublicate']['save_folder'], $config['dublicate']['options']);
                }
            } elseif (in_array($config['type'], ['file-multiple', 'image-multiple']) && $value) {
                continue;
            } elseif ($config['type'] == 'select-multiple') {
                continue;
            } elseif ($config['type'] == 'checkbox') {
                $item->{$field} = $value ? 1 : 0;
            }
        }

        $item->save();

        foreach (config('undefined-pages.' . $request->page . '.form') as $field => $config) {

            $value = $request->{$field};

            if (in_array($config['type'], ['file-multiple', 'image-multiple']) && $value && count($value) > 0) {
                $model = new $config['save']['model'];
                $model->where($config['save']['relation_field'], $item->id)->delete();
                foreach ($value as $file) {
                    $model = new $config['save']['model'];
                    $model->{$config['save']['relation_field']} = $item->id;
                    $model->{$config['save']['field']} = ImageHelper::process($file, $config['save']['folder'], $config['options'] ?? null);
                    if (isset($config['dublicate'])) {
                        $model->{$config['dublicate']['field']} = ImageHelper::process($file, $config['dublicate']['save_folder'], $config['dublicate']['options']);
                    }
                    $model->save();
                }
            } elseif ($config['type'] == 'select-multiple' && $value && count($value) > 0) {
                $model = new $config['save']['model'];
                $model->where($config['save']['relation_field'], $item->id)->delete();
                foreach ($value as $option) {
                    $model = new $config['save']['model'];
                    $model->{$config['save']['relation_field']} = $item->id;
                    $model->{$config['save']['field']} = $option;
                    $model->save();
                }
            }
        }

        return [
            'success' => true,
            'redirect' => config('undefined-pages.' . $request->page . '.in_modal') ? false : '/admin/' . $request->page,
            'close' => true,
            'noty' => ['text' => __('undefined::core.Successfully saved'), 'type' => 'success']
        ];
    }


    public function delete(Request $request)
    {
        $this->middleware('undefined-permissions:admin.' . $request->page . '.delete');

        $model = new $request->model;
        $model->find($request->id)->delete();

        return ['success' => true];
    }
}
