<?php

namespace ScaryLayer\Undefined\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index()
    {
        return view('undefined::login', ['sidebar_hide' => true]);
    }


    public function post(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $user = Auth::user();
            $user->last_ip = $request->ip();
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            if ($user->new) {
                return redirect('/admin/change-password');
            }

            return redirect(session('request-url') ?? '/admin');
        }

        return redirect()->back()->with('error', __('undefined::core.Failed to authenticate user with this credentials'));
    }


    public function change_password()
    {
        if (!Auth::user()->new) {
            return redirect('/admin');
        }

        return view('undefined::change-password');
    }


    public function change_password_post(Request $request)
    {
        Auth::user()->password = Hash::make($request->password);
        Auth::user()->new = 0;
        Auth::user()->save();

        return redirect('/admin');
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}