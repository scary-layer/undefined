<?php

namespace ScaryLayer\Undefined\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ScaryLayer\Undefined\Models\Group;
use ScaryLayer\Undefined\Models\Permission;
use ScaryLayer\Undefined\Service\Config;

class GroupsController extends Controller
{
    public function index()
    {
        $this->middleware('undefined-permissions:admin.groups.view');

        $data = Group::list();

        return view('undefined::pages.groups.index', [
            'title' => __('undefined::core.Groups'),
            'page' => 'groups',
            'in_modal' => false,
            'data' => $data,
            'permissions_count' => Permission::count(),
        ]);
    }


    public function edit(Request $request)
    {
        abort_if(config('undefined-groups.'. $request->name .'.god') && !auth()->user()->isGod(), 404);

        return view('undefined::pages.groups.edit', [
            'title' => $request->name ? __('undefined::core.Edit group') : __('undefined::core.Add group'),
            'page' => 'groups',
            'model' => Group::find($request->name),
            'permissions' => Permission::list(),
        ]);
    }


    public function save(Request $request)
    {
        Group::save($request->all());
        return ['success' => true, 'redirect' => '/admin/groups'];
    }


    public function delete(Request $request)
    {
        Group::delete($request->id);
        return ['success' => true];
    }
}