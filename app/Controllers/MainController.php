<?php

namespace ScaryLayer\Undefined\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ScaryLayer\Undefined\Models\Setting;
use ScaryLayer\Undefined\Service\ImageHelper;

class MainController extends Controller
{
    public function index()
    {
        return view('undefined::index');
    }


    public function ui()
    {
        return view('undefined::ui', ['title' => __('undefined::core.UI elements')]);
    }


    public function upload_image(Request $request)
    {
        $path = ImageHelper::process($request->fileToUpload, 'wysiwyg');
        return ['success' => true, 'file' => env('APP_URL') . '/' . $path];
    }
}