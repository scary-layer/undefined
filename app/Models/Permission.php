<?php

namespace ScaryLayer\Undefined\Models;

use Arr;

class Permission 
{
    static $config = 'undefined-permissions';

    public static function count()
    {
        return collect(Arr::dot(config(self::$config)))->filter(function($value, $key) {
            return !strpos($key, '.text');
        })->count();
    }

    public static function list()
    {
        return config(self::$config);
    }
}