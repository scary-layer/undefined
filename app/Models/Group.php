<?php

namespace ScaryLayer\Undefined\Models;

use Arr;
use ScaryLayer\Undefined\Service\Config;

class Group
{
    static $config = 'undefined-groups';

    public static function may($permission, $group)
    {
        if (config(self::$config . '.' . $group . '.god') == true) {
            return true;
        }

        $parts = explode('.', $permission);

        $action = null;
        $permission = self::$config . '.' . $group . '.permissions';

        foreach ($parts as $key => $part) {
            if ($key + 1 != count($parts) || count($parts) == 1) {
                $permission .= '.'. $part;
            } elseif(count($parts) > 1) {
                $action = $part;
            }
        }

        return in_array($action, config($permission) ?? [])
            || config($action ? $permission . '.' . $action : $permission);
    }

    public static function isGod($group)
    {
        return config(self::$config . '.' . $group .'.god');
    }

    public static function count($group)
    {
        if (config(self::$config . '.'. $group . '.god')) {
            return Permission::count();
        }

        return collect(Arr::dot(config(self::$config . '.'. $group . '.permissions')))->count();
    }

    public static function find($name)
    {
        return config(self::$config .'.'. $name);
    }

    public static function list()
    {
        return config(self::$config);
    }

    public static function listForSelect()
    {
        $config = config(self::$config);

        $i = 0;
        $result = [];
        foreach ($config as $key => $group) if (!Arr::get($group, 'god') || auth()->user()->isGod()) {
            $result[$i]['value'] = $key;
            $result[$i]['text'] = __('undefined::core.'. $group['name']);
            $i++;
        }

        return $result;
    }

    public static function delete($name)
    {
        $group = config('undefined-groups');
        unset($group[$name]);

        Config::edit($group, 'undefined-groups');
    }

    public static function save($data)
    {
        $group = config('undefined-groups');
        $group[$data['key']]['name'] = $data['name'];

        $permissions = [];
        if (isset($data['permissions'])) foreach ($data['permissions'] as $permission => $value) {

            $parts = explode('.', $permission);
            $last = array_pop($parts);

            $path = implode('.', $parts);

            $item = [];
            Arr::set($item, $path, [$last]);

            $permissions = array_merge_recursive($permissions, $item);

            if (in_array($last, ['add', 'edit']) && !in_array('save', Arr::get($permissions, $path))) {
                $item = [];
                Arr::set($item, $path, ['save']);
                $permissions = array_merge_recursive($permissions, $item);
            }
        }

        $group[$data['key']]['permissions'] = $permissions;
        Config::edit($group, 'undefined-groups');
    }
}