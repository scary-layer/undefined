<?php

namespace ScaryLayer\Undefined\Middleware;

use Closure;

class Permissions
{
    public function handle($request, Closure $next, $permission)
    {
        $page = $request->page_name ?? $request->page;
        abort_if($page && !config('undefined-pages.'. $page) && $page != 'settings', 404);
        abort_if(!auth()->user()->may($permission), 403);
        
        return $next($request);
    }
}