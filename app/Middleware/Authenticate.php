<?php

namespace ScaryLayer\Undefined\Middleware;

use Auth;
use Closure;
use Session;

class Authenticate
{
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            Session::put('request-url', $request->path());
            return redirect('/admin/login');
        }

        return $next($request);
    }
}