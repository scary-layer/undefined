<?php

namespace ScaryLayer\Undefined\Middleware;

use Auth;
use Closure;

class Guest
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return redirect('/admin');
        }

        return $next($request);
    }
}