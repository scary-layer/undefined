<?php

namespace ScaryLayer\Undefined\Traits;

use ScaryLayer\Undefined\Models\Group;

trait Permissions
{
    public function may($permission)
    {
        return Group::may($permission, $this->group);
    }

    public function isGod()
    {
        return Group::isGod($this->group);
    }
}