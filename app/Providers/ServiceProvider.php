<?php

namespace ScaryLayer\Undefined\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Arr;
use ScaryLayer\Undefined\Service\BladeShortcuts;
use ScaryLayer\Undefined\Service\Toolbox;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function register()
    {
        
    }


    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'undefined');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'undefined');

        $this->publishes([
            __DIR__.'/../../config/app.php'                     => config_path('undefined.php'),
            __DIR__.'/../../config/controllers.php'             => config_path('undefined-controllers.php'),
            __DIR__.'/../../config/groups.php'                  => config_path('undefined-groups.php'),
            __DIR__.'/../../config/menus.php'                   => config_path('undefined-menus.php'),
            __DIR__.'/../../config/pages.php'                   => config_path('undefined-pages.php'),
            __DIR__.'/../../config/permissions.php'             => config_path('undefined-permissions.php'),
            __DIR__.'/../../config/settings.php'                => config_path('undefined-settings.php'),

            __DIR__.'/../../resources/publishable/css'          => public_path('assets/undefined/css'),
            __DIR__.'/../../resources/publishable/fonts'        => public_path('assets/undefined/fonts'),
            __DIR__.'/../../resources/publishable/img'          => public_path('assets/undefined/img'),
            __DIR__.'/../../resources/publishable/js'           => public_path('assets/undefined/js'),
            __DIR__.'/../../resources/publishable/svg'          => public_path('assets/undefined/svg'),
            __DIR__.'/../../resources/publishable/uploads'      => public_path('uploads'),
            __DIR__.'/../../resources/publishable'              => public_path('assets/undefined'),

            __DIR__.'/../../resources/lang'                     => resource_path('lang/vendor/undefined')
        ]);

        $this->app['router']->aliasMiddleware('undefined-auth',         \ScaryLayer\Undefined\Middleware\Authenticate::class);
        $this->app['router']->aliasMiddleware('undefined-guest',        \ScaryLayer\Undefined\Middleware\Guest::class);
        $this->app['router']->aliasMiddleware('undefined-permissions',  \ScaryLayer\Undefined\Middleware\Permissions::class);

        $loader = AliasLoader::getInstance();
        
        $loader->alias('Toolbox', Toolbox::class);
        $loader->alias('Arr', Arr::class);

        BladeShortcuts::boot();
    }
}