<?php

namespace ScaryLayer\Undefined\Service;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Intervention\Image\Facades\Image;


class ImageHelper
{
    const SAVE_FOLDER = 'uploads';

    public static function process($file, $save_path, $options = [])
    {
        $img = Image::make($file);

        $file_name = sha1(microtime()) . '-' . str_slug(explode('.', $file->getClientOriginalName())[0], '-');
        $destination_path = public_path() . '/'. self::SAVE_FOLDER .'/' . $save_path;

        if(Arr::get($options, 'width') || Arr::get($options, 'height')) {
            $img->resize(Arr::get($options, 'width'), Arr::get($options, 'height'), function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $img->save($destination_path . '/' . $file_name);

        return '/'. self::SAVE_FOLDER .'/' . $save_path . '/' . $file_name;
    }
}