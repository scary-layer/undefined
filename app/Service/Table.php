<?php

namespace ScaryLayer\Undefined\Service;

use Session;
use Illuminate\Http\Request;

class Table
{
    static $paginate = 10;

    public static function process(Request $request, $page, $model)
    {
        $searchable = config('undefined-pages.'. $page . '.searchable');

        if ($request->has('search')) {

            $model = $model->where(function($query) use ($searchable, $request) {

                if (isset($searchable['default']) && count($searchable['default'])) foreach ($searchable['default'] as $field) {
                    $query = $query->orWhere($field, $request->search);
                }

                if (isset($searchable['like']) && count($searchable['like'])) foreach ($searchable['like'] as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $request->search . '%');
                }

                return $query;
            });

        }

        if ($request->has('sort')) {
            $model = $model->orderBy($request->sort, $request->order ?? 'asc');
        }
        
        if ($request->has('count')) {
            self::$paginate = $request->count;
            Session::put('table-count', $request->count);
        } elseif (session('table-count')) {
            self::$paginate = session('table-count');
        }

        $items = $model->paginate(self::$paginate);

        return $items;
    }
}