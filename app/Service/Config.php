<?php

namespace ScaryLayer\Undefined\Service;

class Config
{
    public static function add($item, $filename = 'undefined')
    {
        $items = config($filename);
        $items = array_merge_recursive($items, $item);

        self::edit($items, $filename);
    }


    public static function set($item, $filename = 'undefined')
    {
        $items = config($filename);
        $items = array_replace_recursive($items, $item);

        self::edit($items, $filename);
    }


    public static function edit($items, $filename = 'undefined')
    {
        $file = config_path() . '/'. $filename .'.php';
        file_put_contents($file, "<?php\n\nreturn " . self::var_export($items) . ';');
    }


    public static function var_export($expression)
    {
        $export = var_export($expression, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);

        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);

        $export = join(PHP_EOL, array_filter(["["] + $array));
        return $export;
    }


    public static function generateValidation($page, $id = null)
    {
        $result = [];
        $fields = config('undefined-pages.'. $page .'.form');

        foreach ($fields as $name => $data) if (isset($data['validation'])) {

            $result[$name] = $data['validation'];

            if ($id && mb_strpos(' '. $data['validation'], 'unique')) {

                $parts = explode('|', $data['validation']);
                if (count($parts) > 1) foreach ($parts as $i => $part) if (mb_strpos(' '. $part, 'unique')) {
                    $parts[$i] = $part . ',' . $id;
                }

                $result[$name] = implode('|', $parts);
            }
        }

        return $result;
    }
}