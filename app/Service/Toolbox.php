<?php

namespace ScaryLayer\Undefined\Service;

class Toolbox
{
    public static function printCode($html)
    {
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $dom->loadHTML($html,LIBXML_HTML_NOIMPLIED);
        $result = e($dom->saveXML($dom->documentElement));

        $result = str_replace('=&amp;gt;', '=>', $result);

        return '<pre>'. $result .'</pre>';
    }

    public static function renderBladeString($string)
    {
        $php = \Blade::compileString($string);
        $php = mb_substr($php, 10, -2);

        return 'return '. $php;
    }

    public static function listForSelect($model, $valueField, $textField, $order = 'id', $orderDirection = 'asc')
    {
        $class = new $model;
        return $class->select([$valueField .' as value', $textField .' as text'])->orderBy($order, $orderDirection)->get();
    }
}