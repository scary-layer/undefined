<?php

namespace ScaryLayer\Undefined\Service;

use Blade;

class BladeShortcuts
{
    public static function boot()
    {
        Blade::include('undefined::components.blade.checkbox', 'ucheckbox');
        Blade::include('undefined::components.blade.dropdown-select', 'dropdownselect');
        Blade::include('undefined::components.blade.file', 'ufile');
        Blade::include('undefined::components.blade.file-multiple', 'ufilemultiple');
        Blade::include('undefined::components.blade.radio', 'uradio');
        Blade::include('undefined::components.blade.table', 'utable');
        Blade::include('undefined::components.blade.page-title', 'utitle');
    }
}