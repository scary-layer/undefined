<?php

use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Admin Web Routes
|--------------------------------------------------------------------------
*/

Route::namespace('\\ScaryLayer\\Undefined\\Controllers')->middleware('web')->group(function () {

    /*
    |--------------------------------------------------------------------------
    | Login
    |--------------------------------------------------------------------------
    */

    Route::middleware('undefined-guest')->group(function () {

        Route::get('/admin/login', 'LoginController@index');
        Route::post('/admin/login', 'LoginController@post');
    });


    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */

    Route::middleware('undefined-auth', 'undefined-permissions:admin')->group(function () {

        /*
        |--------------------------------------------------------------------------
        | CUSTOM
        |--------------------------------------------------------------------------
        */

        $pages = config('undefined-controllers');
        if ($pages && count($pages)) foreach (config('undefined-controllers') as $route => $options) {

            if (Arr::get($options, 'type', 'get') == 'get') {
                Route::get('/admin/' . $route, $options['controller'])->middleware($options['middleware'] ?? null);
            } elseif (Arr::get($options, 'type', 'get') == 'post') {
                Route::post('/admin/' . $route, $options['controller'])->middleware($options['middleware'] ?? null);
            } elseif (Arr::get($options, 'type', 'get') == 'any') {
                Route::any('/admin/' . $route, $options['controller'])->middleware($options['middleware'] ?? null);
            }
        }


        /*
        |--------------------------------------------------------------------------
        | GET
        |--------------------------------------------------------------------------
        */

        Route::get('/admin', 'MainController@index');

        Route::get('/admin/change-password', 'LoginController@change_password');
        Route::get('/admin/logout', 'LoginController@logout');

        Route::get('/admin/groups', 'GroupsController@index')->middleware('undefined-permissions:admin.groups.add');
        Route::get('/admin/groups/edit', 'GroupsController@edit')->middleware('undefined-permissions:admin.groups.edit');
        Route::get('/admin/settings', 'SettingsController@index')->middleware('undefined-permissions:admin.settings.view');
        Route::get('/admin/ui', 'MainController@ui');


        /*
        |--------------------------------------------------------------------------
        | POST
        |--------------------------------------------------------------------------
        */

        Route::post('/admin/change-password', 'LoginController@change_password_post');
        Route::post('/admin/groups/save', 'GroupsController@save')->middleware('undefined-permissions:admin.groups.save');
        Route::post('/admin/groups/delete', 'GroupsController@delete')->middleware('undefined-permissions:admin.groups.delete');
        Route::post('/admin/settings/save', 'SettingsController@save')->middleware('undefined-permissions:admin.settings.save');
        Route::post('/admin/upload/image', 'MainController@upload_image')->middleware('undefined-permissions:admin.upload.image');


        /*
        |--------------------------------------------------------------------------
        | GLOBALS
        |--------------------------------------------------------------------------
        */

        Route::any('/admin/{page_name}', 'GlobalController@index');
        Route::get('/admin/{page}/edit', 'GlobalController@edit');

        Route::post('/admin/{page}/save', 'GlobalController@save');
        Route::post('/admin/{page}/delete', 'GlobalController@delete');
    });
});
